from datetime import datetime
import json
import sys
from keyvalues import KeyValues, load_keyvalues


def kv_todict(keyvalues):
    base = dict()
    for k, v in keyvalues.iteritems():
        base[k] = kv_todict(v) if isinstance(v, KeyValues) else str(v)
    return base


def configure():
    if len(sys.argv) < 2:
        print "Usage vkv2json VPKFILE OUTPUT"
        print "Convert a Valve Keyvalue file to a json file"
        exit(-1)


def run():
    with open(sys.argv[2], 'w+') as output:
        output.writelines(("# File generated at %s using vkv2json.py.\n" % datetime.utcnow(),
                          "# this file can be imported in mongoDB with the mongoimport tool.\n"))
        kv = load_keyvalues(sys.argv[1])
        # let's keep a lil' comment with the root element name
        output.write("# %s\n" % kv.name)
        output.write("[")
        for k, v in kv.iteritems():
            if not isinstance(v, KeyValues):
                continue
            data = {'_type': k}
            data.update(kv_todict(v))
            json.dump(data, output)
        output.write("]")


if __name__ == "__main__":
    configure()
    run()