import glob
import datetime
import os
import sys
from subprocess import call

sprite_size = 32
convert = os.getenv("IMAGE_MAGICK_CONVERT") or "convert"


def write_header(fp):
    fp.write("/* This file has been generated using montage.py at %s. */\n" % datetime.utcnow())
    fp.write("/* It contains the CSS definition of every minihero.*/\n")
    fp.write("/* Usage: <div class=\"minihero minihero-abaddon\"></div>\n")


def css_base(fp):
    fp.write(".minihero {\n")
    fp.write("\tdisplay: inline-block;\n")
    fp.write("\tfloat: left;\n")
    fp.write("\tbackground: url('/images/sprite.png');\n")
    fp.write("\twidth: 32px;\n")
    fp.write("\theight: 32px;\n")
    fp.write("\topacity: 0;\n")
    fp.write("}\n")
    fp.write(".minihero[class*=minihero-] { opacity: 1; }\n")


def css_minihero(fp, name, offset):
    fp.write(".minihero.minihero-%s { background-position: %dpx 0px; }\n" % (name, offset))


def configure():
    if len(sys.argv) < 2:
        print "Usage montage IMAGES_DIR OUTPUT"
        print "Scan the IMAGES_DIR for .png files, and build a CSS file for spriting."
        exit(-1)


def generate_css(images_dir, css_path):
    with open(css_path, 'w+') as f_meta:
        css_base(f_meta)
        last_entry_index = 0
        for image in sorted(glob.iglob(os.path.join(images_dir, '*.png'))):
            name = os.path.basename(image).split('.')[0]
            offset = last_entry_index * sprite_size
            css_minihero(f_meta, name, -offset)
            last_entry_index += 1


def generate_sprite(images_dir):
    images_glob = os.path.join(images_dir, '*.png')
    sprite_path = os.path.join(images_dir, '../sprite.png')
    call([convert, images_glob, '+append', sprite_path])


def run(images_dir='./', css_path='./miniheroes.css'):
    generate_sprite(images_dir)
    generate_css(images_dir, css_path)


if __name__ == "__main__":
    configure()
    run(sys.argv[1], sys.argv[2])