from collections import namedtuple
import subprocess
from pymongo import MongoClient

__author__ = 'micael'


class ReplayState:
    NONE = 0
    NEW, PARSING, PARSED, ERROR = range(4)

    def parsed(self):
        return self.NEW, self.PARSING, self.PARSED

    def unparsed(self):
        return self.NONE, self.ERROR


class BountyParser:
    """
    :type mongodb_client: MongoClient
    """
    def __init__(self, mongodb_client, database="dota2_matches"):
        self.client = mongodb_client
        self.db = mongodb_client[database]

    def is_parsed(self, matchid):
        replay_info = self.db.replay.find_one({"matchid": matchid})
        if replay_info is None:
            return False

        return replay_info.state in ReplayState.parsed()


    def parse(self, file):
        subprocess.Popen(["java", "-jar", "bin/simple.jar", file])


    def remove(self, matchid):
        self.db.replay.remove({"matchid": matchid})
        self.db.replay_bounty.remove({"matchid": matchid})