from eve import Eve
import pymongo
import os
from settings import *
from flask import Flask, request, jsonify
from werkzeug.serving import run_simple
from werkzeug.utils import secure_filename, redirect
from werkzeug.wsgi import DispatcherMiddleware
from src.bson_encoder import BSONEncoder
from src.parser import BountyParser

from flask_cache import Cache

api = Eve()
ui = Flask(__name__, static_url_path='')
ui.json_encoder = BSONEncoder

cache = Cache(ui, config={
    'CACHE_TYPE': 'redis',
    'CACHE_REDIS_URL': 'redis://localhost:6379/2',
})

# === Flask shits ===
# upload a new replay
@ui.route("/submit", methods=['POST'])
def submit_replay():
    f = request.files['replay']
    matchid, ext = f.filename.split(".")
    if not ext == "dem":
        return "Bad file type, please upload a Dota2 replay file.", 403

    storage_path = os.path.join("data", secure_filename(f.filename))
    f.save(storage_path)
    analyse(matchid, storage_path)
    return redirect("index.html#%s" % matchid)  # reload


@ui.route("/bounty/<int:matchid>")
@cache.cached(timeout=3600*24)
def get_bounties(matchid):
    client = pymongo.MongoClient(MONGO_HOST, MONGO_PORT)
    db = client[MONGO_DBNAME]
    response = {
        '_items': [bounty for bounty in db.replay_bounty.find({"matchid": matchid}, {"tick": 1, "net_worth": 1, "playerId": 1}).sort("tick", 1)]
    }
    return jsonify(response)


def analyse(matchid, file):
    parser = BountyParser(pymongo.MongoClient(MONGO_HOST, MONGO_PORT), database=MONGO_DBNAME)
    if parser.is_parsed(matchid):
        raise ValueError("Match is already parsed!")
    parser.parse(file)


app = DispatcherMiddleware(ui, {
    '/api': api
})

if __name__ == "__main__":
    run_simple("0.0.0.0", 5000, app,
               use_reloader=True)
