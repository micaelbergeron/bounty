/**
 * User: micael
 * Date: 14-09-27
 * Time: 11:13
 */

var osti = {};

osti.utils = {
    arrayClear: function(array) {
        while(array.length) { array.pop(); }
    }
    ,spin: function(options) {
        var opts = {
            lines: 13, // The number of lines to draw
            length: 20, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#000', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 37, // Afterglow percentage
            shadow: true, // Whether to render a shadow
            hwaccel: true, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent
            left: '50%' // Left position relative to parent
        };
        return new Spinner($.extend({}, opts, options)).spin();
    }
}

osti.const = {
    TICK_PER_SECONDS: 30,
    PLAYER_COLORS: [
        // radiant
        d3.rgb(46,106,230)      // blue
        ,d3.rgb(93,230,173)     //"teal"
        ,d3.rgb(173,0,173)      //"purple"
        ,d3.rgb(220,217,10)     //"yellow"
        ,d3.rgb(230,98,0)       //"orange"

        // dire
        ,d3.rgb(230,122,176)    //"pink"
        ,d3.rgb(146,164,64)     //"lightgreen"
        ,d3.rgb(92,197,224)     //"lightblue"
        ,d3.rgb(0,119,31)       //"green"
        ,d3.rgb(149,96,0)       //"brown"
    ]
};

osti.bountyPlotter = function(selector) {

    var margin = {top: 20, right: 0, bottom: 20, left: 50},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var $plot = d3.select(selector);
    
    this.timeFromTick = function(t) {
        var formatter = d3.format("02d");
        var seconds = t / 30;

        var hours = Math.floor(seconds / 3600);
        var minutes = Math.floor((seconds % 3600) / 60);
        var seconds = Math.floor(((seconds % 3600) % 60));

        var retval = formatter(minutes) + ":" + formatter(seconds);
        if (hours > 0)
            retval = formatter(hours) + ":" + retval;

        return retval;
    };

    function customAxis(g) {
        g.selectAll("text")
            .attr("x", 4)
            .attr("dy", -4);
    };
    
    this.plot = function(maximas, data) {
        var y = d3.scale.linear()
            .domain([0, maximas.y])
            .range([height, 0]);

        var x = d3.scale.linear()
            .domain([0, maximas.x])
            .range([0, width]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(6, 0)
            .ticks(30)
            .tickFormat(this.timeFromTick)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .ticks(20)
            .tickSize(width)
            .orient("right");

        this.get_plot().empty();
        var svg = $plot
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var gy = svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .call(customAxis);

        var gx = svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        var f_line = d3.svg.line()
            .x(function(d) { return x(d.tick); })
            .y(function(d) { return y(d.net_worth); });

        var f_reliable_line = d3.svg.line()
            .x(function(d) { return x(d.tick); })
            .y(function(d) { return y(d.reliable); });

        var gpath = svg.append("g")
            .attr("class", "gpath")
            .selectAll("path")
            .data([0,1,2,3,4,5,6,7,8,9]);

        var path_enter = gpath.enter();
        path_enter.append("path")
            .attr("data-player-id", function(d) { return d; })
            .attr("d", function(d) { return f_line(data[d] || []); })
            .attr("stroke-width", "2")
            .attr("stroke", function(d) { return osti.const.PLAYER_COLORS[d]; })
            .attr("fill", "none");

        // reliable
        /*
        path_enter.append("path")
            .attr("data-player-id", function(d) { return d; })
            .attr("d", function(d) { return f_reliable_line(DATA[d]); })
            .attr("stroke", function(d) { return playerColors[d].brighter(2); })
            .attr("fill", "none");
        */
    };
    
    this.get_plot = function() { return $(selector); }
};

osti.controller = function(url) {
    this.url = url;
        
    function handleError(err) {
        console.log(err);
    }    
}

osti.matchController = function(url) {
    this.url = url;
    this.replay = null;
    this.$elem = {
        pnl_players: $('#panel-button')
        ,plot_net_worth: $('#plot-networth')
    }
    var self = this;
    var players = [];
    var bounties = [];
    var maximas = { x: 0, y: 0 };
    var plotter = new osti.bountyPlotter('#plot-networth');
    
    this.test = function() {
        console.log("hello!");
    };
    
    this.clearPlayers = function() {
        this.$elem.pnl_players.hide();
    }
    
    this.showPlayers = function() {
        if (players.length === 0)
            return;
        
        this.$elem.pnl_players.find('.player-buttons button').each(function() {
            var $this = $(this);
            var player = players[$this.data('playerId')];

            var $minihero = $this.find('.minihero').removeClass().addClass('minihero');

            if (player.hasOwnProperty('name'))
                $this.find('.player-name').text(player.name);


            if (player.hasOwnProperty('hero'))
                $minihero.addClass("minihero-" + player.hero.replace("npc_dota_hero_", ""));
        });
        this.$elem.pnl_players.show();
    }
        
    this.update = function() {
        plotter.plot(maximas, bounties);
    }
    
    this.reset = function() {
        plotter.plot({ x: 100000, y: 25000 }, []);
    }
    
    this.load = function(matchid) {
        self.clearPlayers();
                        
        var q_meta = 'replay?where={"matchid":'+matchid+'}';
        $.getJSON('/api/'+ q_meta, null, function(json) {
            self.replay = json._items[0];            
            for (var i = 0; i < 10; i++)
                players[i] = self.replay['player'+i];
            
            self.showPlayers();
        });
        
        var spinner = osti.utils.spin();
        $('.plot-networth').append(spinner.el);
        
        //var q_bounties = 'replay_bounty?where={"matchid":'+matchid+'}&sort=[("tick", 1)]';
        $.getJSON('/bounty/' + matchid, null)
        .done(function(json) {
            osti.utils.arrayClear(bounties);
            for (var i in json._items)
            {
                var obj = json._items[i];
                maximas.x = Math.max(obj.tick, maximas.x);
                maximas.y = Math.max(obj.net_worth, maximas.y);

                if (bounties[obj.playerId] === undefined) bounties[obj.playerId] = [];
                bounties[obj.playerId].push(obj);
            }
            self.update();
        })
        .always(function() { spinner.stop(); });
    };   
};

function init() {
    var controllers = {
        match: new osti.matchController()
    };

    controllers.match.test();
    //controllers.match.reset();

    // bind events
    $('#btn-load').on('click', function(ev) {
        var matchid = $('#txt-matchid').val();
        if (matchid.length > 7)
            controllers.match.load(matchid);
    });

    $('#btn-save').on('click', function(ev) {
        // update the title
    });
    
    $('.team-button').on('click', function(ev) {
        var $team_button = $(this);
        var team_buttons = $team_button.parents('.team-buttons-container').find('.player-buttons button').each(function() {
            var $player_button = $(this);
            if ($team_button.hasClass('off') === $player_button.hasClass('off')) // XNOR
                $player_button.click();
        });
        
        $team_button.toggleClass('off');
    });
            
    for (var i = 0; i < 10; i++) {
        var $icon = $(document.createElement("div"));
        $icon.addClass('minihero');

        var $player_name = $(document.createElement('span'));
        $player_name.addClass('player-name ellipsis');
        // $player_name.text();

        $button = $(document.createElement("button"));
        $button.css({ background: osti.const.PLAYER_COLORS[i] });
        $button.addClass('pure-button button-toggle');
        $button.data("playerId", i);
                
        $button.on("click", function() {
           $("path[data-player-id='" + $(this).data('playerId') +"']").toggle('slow');
           $(this).toggleClass("off");
        });
        
        var panel = i < 5 ? '#radiant-buttons' : '#dire-buttons';
        $player_name.appendTo($button);
        $icon.appendTo($button);
        $button.appendTo($(panel).find('.player-buttons'));
    }

    // # data
    var hash = window.location.hash.substring(1);
    if (hash.length) {
        $('#txt-matchid').val(hash);
        $('#btn-load').click();
    }
}

$(document).on('ready', init);
