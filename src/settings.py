MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DBNAME = "dota2_matches"

PYMEMCACHE = {
    'server': ('localhost', 11211),
    'no_delay': True,
}

replay = {
    'schema': {
        'matchid': {'type': 'string'},
        'net_worth': {'type': 'integer'},
        'playerId': {'type': 'integer'}
    }
}

DOMAIN = {
    'replay': { },
    'replay_bounty': {
        'pagination': False,
        'hateoas': False,
        'extra_response_fields': []
    }
}