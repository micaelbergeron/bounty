# Dota2-Bounty
Dota2-Bounty (referred as Bounty) is a tool for real-time analysis and projection of the new gold bounty system in Dota2 6.82.

## The math behind it
### Old formulas
#### XP

VictimLevel = Level of the Victim

1 Hero: XP = 120 + 20 * VictimLevel
2 Heroes: XP = 90 + 15 * VictimLevel
3 Heroes: XP = 30 + 7 * VictimLevel
4 Heroes: XP = 20 + 5 * VictimLevel
5 Heroes: XP = 15 + 4 * VictimLevel

#### Gold
VictimLevel = Level of the Victim

1 Assist: Gold = 125 + 12 * VictimLevel
2 Assist: Gold = 40 + 10 * VictimLevel
3 Assist: Gold = 10 + 6 * VictimLevel
4+ Assist: Gold = 6 + 6 * VictimLevel

### New formulas
#### XP
This change only affects the extra XP given if within an area after a kill. 
It **does not affect** the natural XP you get for killing a hero of a certain Level. 

VictimLevel = Level of the Victim
VictimXP = Total XP of the Victim
EnemyTeamXP = Enemy team's total XP
AlliedTeamXP = Your team's total XP
XPDifference = ( EnemyTeamXP - AlliedTeamXP )/ ( EnemyTeamXP + AlliedTeamXP ) (minimum 0)
XPFactor = XPDifference * VictimXP

1 Hero: XP = 20 * VictimLevel + XPFactor * 0.5
2 Heroes: XP = 15 * VictimLevel + XPFactor * 0.35
3 Heroes: XP = 10 * VictimLevel + XPFactor * 0.25
4 Heroes: XP = 7 * VictimLevel + XPFactor * 0.2
5 Heroes: XP = 5 * VictimLevel + XPFactor * 0.15

#### Gold
This change only affects the extra Gold given if within an area after a kill. 
It does not affect the natural Gold received for killing a hero of a certain Level/Streak. 

**The player that got the last hit now also gets the area of effect bounty.**

VictimLevel = Level of the Victim
VictimNW = The victim's Net Worth
EnemyTeamNW = Enemy team's total Net Worth
AlliedTeamNW = Your team's total Net Worth
NWDifference = ( EnemyTeamNW - AlliedTeamNW )/ ( EnemyTeamNW + AlliedTeamNW ) (minimum 0)
NWFactor = NWDifference * VictimNW

1 Hero: Gold = 40 + 7 * VictimLevel + NWFactor * 0.5
2 Heroes: Gold = 30 + 6 * VictimLevel + NWFactor * 0.35
3 Heroes: Gold = 20 + 5 * VictimLevel + NWFactor * 0.25
4 Heroes: Gold = 10 + 4 * VictimLevel + NWFactor * 0.2
5 Heroes: Gold = 10 + 4 * VictimLevel + NWFactor * 0.15
 								
**In combination with these changes, the gold for ending a spree is in turn reduced, from 125->1000 to 100->800.**

