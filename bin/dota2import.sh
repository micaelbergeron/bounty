#!/bin/bash

PATH="/c/bin/MongoDB/bin":$PATH
DB=dota2
COLLECTION=heroes

# convert scripts to importable stuff
python ../lib/vkv2json/vkv2json.py ../src/data/scripts/npc/npc_heroes.txt $1

# import it
mongoimport.exe --drop -d $DB -c $COLLECTION --jsonArray < $1
