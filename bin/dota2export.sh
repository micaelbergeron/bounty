#!/bin/bash

DOTA2_VPK_PATH='C:/Program Files (x86)/Steam/SteamApps/common/dota 2 beta/dota/pak01_dir.vpk'
JAVAVPK=javavpk-1.0-SNAPSHOT.jar

function export_all() {
    OUTDIR=$1
    JAVA_ARGS="java -jar $JAVAVPK -o $OUTDIR \"$DOTA2_VPK_PATH\""
    eval $JAVA_ARGS
}

function export_assets() {
    OUTDIR=$1
    PATTERN=$2

    JAVA_ARGS="java -jar $JAVAVPK -p $PATTERN -o tmp \"$DOTA2_VPK_PATH\""

    echo "Running $JAVA_ARGS"

    eval $JAVA_ARGS
    mkdir -p $OUTDIR
    mv tmp/$PATTERN "$OUTDIR/" 
    rm -rf tmp 
    
    echo "Done!"
}

echo "Extracting assets from DOTA2 resources."
export_assets ../src/static/images/miniheroes/ resource/flash3/images/miniheroes/*.png
export_assets ../src/data/scripts/npc/ scripts/npc/*.txt
